import logo from './logo.svg';
import './App.css';


//remove the template in the module, to give way to the new line of code that we will create for the app.


//When compiling multiple components, they should be wrapped inside a sigle element.

//save an image of your favorite cartoon character inside the public folder.

//NEW SKILL : Identify how to access resources and elements from the public folder/library of the project.

//PUBLIC FOLDER -> to store all documents and modules that shared accross all components of the app that are visible to all.

//to acquire the image that you want to display, you have to get it from the public folder. Import the element inside this module


//on line 15, the resource that was target was NOT saved in the public folder, so the file has to be imported for it to become usable.
import cartoon from './shiroe.png';
  //pass down the alias that identifies the resource into our element.

function App() {
  return (
  <div> 
    <h1>Welcome to the course booking batch 164</h1>
    <h5>This is our project in React</h5>
    <h6>Come Visit our website</h6>
  {/*This image came from the /src folder*/}
    <img src={cartoon} alt="image note found"/>
    {/*Let's try to display an image but this time it will come from the public folder*/}
  {/*The resource used on line 33 came from folder*/}
    <img src="/shiroe.png"/>
    <h3>This is My Favorite Cartoon Character</h3>
  </div>
  );
}

export default App;
